<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UsersController@index');

Route::get('/removed', 'UsersController@removed');

Route::get('/add', 'UsersController@create');

Route::post('/add', 'UsersController@store');

Route::get('/{user}', 'UsersController@show');

Route::get('/edit/{user}', 'UsersController@edit');

Route::post('/edit/{user}', 'UsersController@update');

Route::post('/delete/{user}', 'UsersController@destroy');
