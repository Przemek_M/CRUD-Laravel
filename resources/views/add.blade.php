@extends('layout')

@section('navbar')
    @include('partials.navbar')
@endsection

@section('content')
    <form method="POST" action="/add" style="width: 200px; margin: 0 auto">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name" class="control-label">Imię</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>

        <div class="form-group">
            <label for="email" class="control-label">Email</label>
            <input type="email" class="form-control" id="email" name="email">
        </div>

        <div class="form-group" style="text-align: center">
            <div class="btn-group">
                <a href="/" class="btn btn-danger">Cofnij</a>
                <button type="submit" class="btn btn-primary">Dodaj</button>
            </div>
        </div>

    </form>
@endsection