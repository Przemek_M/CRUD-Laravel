<!doctype html>
<html>
<head>

    <title>CRUD</title>
    @yield('head')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style type="text/css">
        #container {
            padding: 15pt 15%;
            margin-top: -15pt;
        }

        #nav {
            padding: 10pt 10%;
        }

        table {
            text-align: center;
            width: 100%;
        }

        table thead tr th {
            text-align: center;
        }
    </style>
</head>
<body>
<div id="nav">
    @yield('navbar')
</div>


<div id="container">
    @yield('content')

</div>
</body>
</html>
