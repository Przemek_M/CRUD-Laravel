@extends('layout')

@section('navbar')
    @include('partials.navbar')
@endsection


@section('content')

    <table id="users_table" class="display">
        <thead>
        <tr>
            <th>Imie</th>
            <th>Email</th>
            <th>Akcje</th>
        </tr>
        </thead>
        <tbody>
        @if(count($users)>0)
        @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    <form method="GET" action="/{{$user->id}}"
                          style="vertical-align: top; display: inline-block">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-info">Szczegóły</button>
                    </form>
                    @include('partials.buttons')
                </td>
            </tr>
        @endforeach
            @else
            <tr><td colspan="3">Brak wpisów</td></tr>
            @endif
        </tbody>
    </table>
@endsection