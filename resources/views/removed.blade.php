@extends('layout')

@section('navbar')
    @include('partials.navbar')
@endsection


@section('content')

    <table id="users_table" class="display">
        <thead>
        <tr>
            <th>Imie</th>
            <th>Email</th>
            <th>Data usunięcia</th>
        </tr>
        </thead>
        <tbody>
        @if(count($users)>0)
            @foreach($users as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->removed_at}}</td>
                </tr>
            @endforeach
        @else
            <tr><td colspan="3">Brak wpisów</td></tr>
        @endif
        </tbody>
    </table>
@endsection