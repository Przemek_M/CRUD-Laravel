@extends('layout')

@section('navbar')
    @include('partials.navbar')
@endsection

@section('content')

    <table id="users_table" class="display">
        <thead>
        <tr>
            <th>Imie</th>
            <th>Email</th>
            <th>Dodano</th>
            <th>Akcje</th>
        </tr>
        </thead>
        <tbody>
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->created_at}}</td>
                    <td>
                        @include('partials.buttons')
                    </td>
                </tr>
        </tbody>
    </table>
@endsection